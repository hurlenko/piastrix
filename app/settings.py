import errno
import json
import logging.config
import os
import pathlib


def configure_logging(
    default_path="configs/logging.json", env_key="FLASK_APP_LOG_CFG"
):
    log_dir = "logs/"
    try:
        os.makedirs(log_dir)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    base_dir = pathlib.Path(__file__).parent.parent
    config_path = base_dir / os.getenv(env_key, default_path)
    with open(config_path.as_posix(), "r", encoding="utf-8") as f:
        config = json.load(f)
    logging.config.dictConfig(config)
