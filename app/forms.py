from flask_wtf import FlaskForm
from wtforms import (
    FloatField,
    SelectField,
    StringField,
    SubmitField,
    ValidationError,
)
from wtforms import validators
from wtforms.widgets import TextArea

from app.providers.piastrix.constants import Currencies


class PaymentForm(FlaskForm):
    amount = FloatField(
        "Amount",
        render_kw={"type": "number", "step": "0.01"},
        validators=[validators.required()],
    )
    currency = SelectField(
        "Currency", choices=Currencies.to_form_choices(), default="USD"
    )
    description = StringField(
        "Description",
        widget=TextArea(),
        validators=[validators.required(), validators.length(max=150)],
    )
    submit = SubmitField("Buy")

    def validate_amount(self, field):
        if field.data <= 0:
            raise ValidationError("Amount must be > 0")
