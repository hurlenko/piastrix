import logging


def get_app_logger(name="piastrix"):
    return logging.getLogger(name)
