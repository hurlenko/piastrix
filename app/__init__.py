from pathlib import Path

from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app import settings

db = SQLAlchemy()
migrate = Migrate()


def create_app():
    app = Flask(__name__)
    app.config.from_pyfile(
        Path(__file__).parent.parent / "configs" / "flask.py"
    )
    app.config.from_envvar("FLASK_APP_SETTINGS", silent=True)
    db.init_app(app)
    migrate.init_app(app, db)
    # Logging setup (stdout + file)
    settings.configure_logging()

    return app


app = create_app()

from app import routes, models
