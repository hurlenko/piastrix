from flask import render_template, request

from app import app, db
from app.forms import PaymentForm
from app.models import Transaction
from app.providers.piastrix.constants import Currencies
from app.providers.piastrix.payments import get_payment_method
from app.utils import get_app_logger

logger = get_app_logger(__name__)


@app.route("/", methods=["GET", "POST"])
@app.route("/index", methods=["GET", "POST"])
def index():
    form = PaymentForm(request.form)
    if request.method == "POST" and form.validate():
        currency_code = Currencies.get_code(form.currency.data)

        if not currency_code:
            logger.warning(f"Invalid currency {form.currency.data}")
            return render_template("index.html", form=form)
        params = {**form.data, **{"currency": currency_code}}
        logger.info(f"Received data from form {params}")
        payment = Transaction(
            amount=params["amount"],
            currency=params["currency"],
            description=params["description"],
        )
        db.session.add(payment)
        db.session.commit()
        piastrix = get_payment_method(currency_code)
        method = piastrix(params, payment.id)
        return method.process_payment()
    return render_template("index.html", title="Enter details", form=form)
