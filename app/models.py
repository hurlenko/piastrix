import datetime

from app import db


class Transaction(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    currency = db.Column(db.String(3))
    amount = db.Column(db.Float)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    description = db.Column(db.String(150))

    def __repr__(self):
        return "<Transaction {}{} {}>".format(
            self.amount, self.currency, self.time
        )
