import os


class Currencies:
    USD = "840"
    EUR = "978"
    RUB = "643"

    @classmethod
    def get_code(cls, currency):
        return getattr(cls, currency, None)

    @classmethod
    def to_form_choices(cls):
        return [[x] * 2 for x in ("USD", "EUR", "RUB")]


PAY_URL = "https://pay.piastrix.com/en/pay"
BILL_URL = "https://core.piastrix.com/bill/create"
INVOICE_URL = "https://core.piastrix.com/invoice/create"
SECRET = os.getenv("PIASTRIX_SECRET")
SHOP_ID = os.getenv("PIASTRIX_SHOP_ID")
PAYWAY = "payeer_rub"
