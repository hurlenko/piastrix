import hashlib
import uuid
from abc import abstractmethod, ABCMeta
from typing import Tuple

import requests
from flask import render_template, redirect

from app.providers.piastrix import constants
from app.providers.piastrix.constants import Currencies
from app.providers.piastrix.exceptions import PiastrixError
from app.utils import get_app_logger

logger = get_app_logger(__name__)


def get_payment_method(currency):
    if currency == Currencies.EUR:
        return PiastrixPay
    elif currency == Currencies.USD:
        return PiastrixBill
    elif currency == Currencies.RUB:
        return PiastrixInvoice
    logger.error(f"No payment mathod for currency: {currency}")
    return None


class PaymentBase(metaclass=ABCMeta):
    def __init__(self, payload, order_id, **kwargs):
        super().__init__(**kwargs)
        self.payload = payload
        self.order_id = order_id
        self.preprocess_request()

    @property
    @abstractmethod
    def KEYS(self) -> Tuple[str]:
        """List of required keys"""

    @property
    @abstractmethod
    def URL(self) -> Tuple[str]:
        """Piastrix processing URL"""

    @abstractmethod
    def preprocess_request(self):
        """Preprocess request payload"""

    @abstractmethod
    def process_payment(self):
        """Payment processing"""

    def request(self, *args, **kwargs):
        return requests.post(*args, **kwargs)

    def generate_signature(self, payload):
        sorted_values = [str(payload[key]) for key in sorted(self.KEYS)]
        string = ":".join(sorted_values) + constants.SECRET
        logger.info(f"Generated string: {string}, {payload}")
        signature = hashlib.sha256(string.encode("utf-8")).hexdigest()
        logger.info(f"Generated signature: {signature}")
        return signature

    def random_order_id(self):
        return str(uuid.uuid4())

    def validate_response(self, response):
        if response.status_code != 200:
            raise PiastrixError(f"Bad response {response.status_code}")
        json_data = response.json()
        logger.info(f"Received response: {json_data}")
        if json_data.get("error_code") != 0 or json_data.get("result") is False:
            logger.error(f"Error processing response: {json_data}")
            raise PiastrixError(
                json_data.get("message"), json_data.get("error_code")
            )

        return json_data


class PiastrixPay(PaymentBase):
    KEYS = ("amount", "currency", "shop_id", "shop_order_id")
    URL = constants.PAY_URL

    def preprocess_request(self):
        self.payload.update(
            {"shop_id": constants.SHOP_ID, "shop_order_id": self.order_id}
        )

    def process_payment(self):
        self.payload.update(
            {"sign": self.generate_signature(self.payload), "url": self.URL}
        )
        logger.info(f"Redirecting user to pay form")
        return render_template("piastrix/pay.html", **self.payload)


class PiastrixBill(PaymentBase):
    KEYS = (
        "payer_currency",
        "shop_amount",
        "shop_currency",
        "shop_id",
        "shop_order_id",
    )
    URL = constants.BILL_URL

    def preprocess_request(self):
        amount = self.payload.pop("amount")
        currency = self.payload.pop("currency")
        self.payload.update(
            {
                "shop_amount": amount,
                "payer_currency": currency,
                "shop_currency": currency,
                "shop_id": constants.SHOP_ID,
                "shop_order_id": self.order_id,
            }
        )

    def process_payment(self):
        self.payload.update({"sign": self.generate_signature(self.payload)})
        response = self.request(self.URL, json=self.payload)
        response = self.validate_response(response)
        redirect_url = response.get("data", {}).get("url")

        if redirect_url:
            logger.info(f"Redirecting user to url: {redirect_url}")
            return redirect(redirect_url)


class PiastrixInvoice(PaymentBase):
    KEYS = ("amount", "currency", "payway", "shop_id", "shop_order_id")
    URL = constants.INVOICE_URL

    def preprocess_request(self):
        self.payload.update(
            {
                "shop_id": constants.SHOP_ID,
                "shop_order_id": self.order_id,
                "payway": constants.PAYWAY,
            }
        )

    def process_payment(self, *args, **kwargs):
        self.payload.update({"sign": self.generate_signature(self.payload)})
        response = self.request(self.URL, json=self.payload)
        response = self.validate_response(response)
        form_data = {
            "method": response["data"]["method"],
            "url": response["data"]["url"],
            "lang": response["data"]["data"]["lang"],
            "m_curorderid": response["data"]["data"]["m_curorderid"],
            "m_historyid": response["data"]["data"]["m_historyid"],
            "m_historytm": response["data"]["data"]["m_historytm"],
            "referer": response["data"]["data"]["referer"],
        }
        return render_template("piastrix/invoice.html", **form_data)
